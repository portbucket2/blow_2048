using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValueInvert : MonoBehaviour
{
    private Slider self_slider;
    public List<Slider> target_sliders;
    public List<Slider> target_sliders_inverted;


    private void Awake()
    {
        self_slider = GetComponent<Slider>();

    }

    private void Update()
    {
        self_slider.onValueChanged.AddListener(delegate { SelfValueChanged(); });
    }

    private void SelfValueChanged()
    {
        foreach (Slider item in target_sliders)
        {
            item.value = self_slider.value;
        }

        foreach (Slider item in target_sliders_inverted)
        {
            item.value = 1 - self_slider.value;
        }
    }
}
