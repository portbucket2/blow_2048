using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BallBehavior : MonoBehaviour
{
    public bool so_dead = false;
    public int id = 0;
    public float scaling_factor = 0.25f;
    public float overall_scaling_factor = 1f;
    public float mass_increase_factor = 5f;
    public float init_mass = 1f;

    public List<Material> id_wise_mats = new List<Material>();
    public TMP_Text number;

    private Rigidbody rb;
    public MeshRenderer mesh;
    public Animator mesh_anim;

    public BallRayCast raycast_check;

    private LevelManager levelManager;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
        rb = GetComponent<Rigidbody>();

        Rescale();
        IdUpdate();
    }

    private void Update()
    {
        number.text = Mathf.Pow(2, id).ToString();
    }


    public void GetPushed(int id, Vector3 push_force)
    {
        if (raycast_check.fan_ray_hit[id] || levelManager.lock_broken)
        {
            rb.AddForce(push_force, ForceMode.Force);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (so_dead)
        {
            return;
        }

        //Vibrator.Vibrate(20);

        if (collision.gameObject.CompareTag("Ball"))
        {
            BallBehavior ball = collision.gameObject.GetComponent<BallBehavior>();

            if (ball.id != id)
            {
                return;
            }

            ball.so_dead = true;
            collision.gameObject.SetActive(false);

            float avg_dist = (collision.transform.position - transform.position).magnitude / 2;
            Vector3 dir = (collision.transform.position - transform.position).normalized;

            transform.position += dir * avg_dist;

            id++;

            Rescale();
            IdUpdate();

            mesh_anim.Play("Squish_Pop", -1, 0);
            Vibrator.Vibrate(65);
        }
    }

    private void Rescale()
    {
        Vector3 scale = Vector3.one * overall_scaling_factor + Vector3.one * id * scaling_factor;

        transform.localScale = scale;

        rb.mass = init_mass + id * mass_increase_factor;
    }

    private void IdUpdate()
    {
        int temp_id = (id % id_wise_mats.Count);
        mesh.material = id_wise_mats[temp_id];
    }
}
