using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragToMove : MonoBehaviour
{
    public Text debug_text;

    private Vector3 touch_offset = Vector3.zero;

    private float touch_z_coord;

    Camera cam;

    private void Awake()
    {
        cam = Camera.main;
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 touchpos = Input.mousePosition;

        touchpos.z = touch_z_coord;

        return cam.ScreenToWorldPoint(touchpos);
    }

    private void OnMouseDown()
    {
        touch_z_coord = cam.WorldToScreenPoint(transform.position).z;

        touch_offset = transform.position - GetMouseWorldPos();
    }

    private void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos() + touch_offset;
    }
    //void Update()
    //{
    //    transform.localPosition -= touch_relative * drag_sensitivity;
    //}
}
