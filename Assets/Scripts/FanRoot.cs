using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanRoot : MonoBehaviour
{
    public bool particle_enabled;

    public List<FanBehavior> pushers;
    public RotateSelf propeller;

    public List<ParticleSystem> particles;

    private float gravity_toggle = 0;

    private LevelManager levelManager;

    public int particle_min;
    public int particle_max;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    private void FixedUpdate()
    {
        Physics.gravity = Vector3.down * 4.095f * levelManager.fan_toggle;

        foreach (ParticleSystem particle in particles)
        {
            particle.maxParticles = (int)(levelManager.fan_toggle * 100) * particle_max / 100;
        }
    }

    public void Toggle()
    {
        //gravity_toggle = 1 - gravity_toggle;

        //Physics.gravity = Vector3.down * 4.095f * gravity_toggle;

        propeller.Toggle();

        foreach (FanBehavior pusher in pushers)
        {
            pusher.Toggle();
        }

        //if (particle_enabled)
        //{
            //particle.maxParticles
            //if (gravity_toggle == 1)
            //{
            //    particle.Play();
            //}
            //else
            //{
            //    particle.Stop();
            //}
        //}
    }

}
