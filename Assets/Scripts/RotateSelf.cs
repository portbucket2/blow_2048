using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSelf : MonoBehaviour
{
    public float speed;

    private float current_speed;
    private int toggle = 0;

    private LevelManager levelManager;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    void Update()
    {
        current_speed = Mathf.Lerp(current_speed, (speed * levelManager.fan_toggle), Time.deltaTime * 2f);

        transform.Rotate(Vector3.forward, Time.deltaTime * current_speed, Space.Self);
    }

    public void Toggle()
    {
        toggle = 1 - toggle;
    }
}
