using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerDeactivateTagged : MonoBehaviour
{
    public string object_tag;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(object_tag))
        {
            other.gameObject.SetActive(false);

            Vibrator.Vibrate(85);
        }
    }
}
